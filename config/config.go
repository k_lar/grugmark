package config

import (
	"grugmark/utils"
	"log"
	"os/user"
	"runtime"
)

var (
	BookmarkStoreLocation string
	BookmarkStoreFile     string
)

func InitConfig() {
	currentUser, err := user.Current()
	if err != nil {
		log.Println("Error:", err)
		return
	}

	homeFolder := currentUser.HomeDir

	switch runtime.GOOS {
	case "windows":
		BookmarkStoreLocation = homeFolder + "/grugmark/"
	default:
		BookmarkStoreLocation = homeFolder + "/.config/grugmark/"
	}

	BookmarkStoreFile = utils.GetEnv("GRUGMARK_PATH", BookmarkStoreLocation+"bookmarks.json")
}
