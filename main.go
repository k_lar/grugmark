package main

import (
	"grugmark/cli"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		cli.AssistedMode()
	} else {
		cli.ParseArgs()
	}
}
