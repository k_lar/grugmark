package cli

import (
	"fmt"
	"grugmark/config"
	"grugmark/models"
	"grugmark/utils"
	"os"
	"strings"
)

var version = "0.1.0"

func ParseArgs() {
	config.InitConfig()
	var newBookmark models.Bookmark
	addingBookmark := false
	arg_num := 1

	for {
		if arg_num >= len(os.Args) {
			os.Exit(0)
		}
		switch os.Args[arg_num] {
		case "-h", "--help":
			Help()

		case "-v", "--version":
			ShowVersion(version)

		case "-D", "--debug":
			fmt.Println("Bookmark store: " + config.BookmarkStoreFile)

		case "-a", "--show-all":
			DisplayBookmarks(config.BookmarkStoreFile)

		case "-s", "--search":
			InteractiveSearchMode(config.BookmarkStoreFile)

		case "-n", "--name":
			addingBookmark = true
			if arg_num+1 >= len(os.Args) || os.Args[arg_num+1] == "" {
				fmt.Println("Empty string.")
			} else {
				newBookmark.Name = (os.Args[arg_num+1])
			}
			arg_num++

		case "-l", "--link":
			addingBookmark = true
			if arg_num+1 >= len(os.Args) || os.Args[arg_num+1] == "" {
				fmt.Println("Empty string.")
			} else {
				newBookmark.Link = (os.Args[arg_num+1])
			}
			arg_num++

		case "-d", "--description":
			addingBookmark = true
			if arg_num+1 >= len(os.Args) || os.Args[arg_num+1] == "" {
				fmt.Println("Empty string.")
			} else {
				newBookmark.Description = (os.Args[arg_num+1])
			}
			arg_num++

		case "-t", "--tags":
			addingBookmark = true
			if arg_num+1 >= len(os.Args) || os.Args[arg_num+1] == "" {
				fmt.Println("Empty string.")
			} else {
				newBookmark.Tags = strings.Fields(os.Args[arg_num+1])
			}
			arg_num++

		case "-c", "--category":
			addingBookmark = true
			if arg_num+1 >= len(os.Args) || os.Args[arg_num+1] == "" {
				fmt.Println("Empty string.")
			} else {
				newBookmark.Category = (os.Args[arg_num+1])
			}
			arg_num++

		default:
			fmt.Printf("Unknown argument \"%s\"\n", os.Args[arg_num])
		}
		arg_num++
		// When no args left, do validation checks
		if arg_num >= len(os.Args) {
			// Check if user wanted to add bookmark
			if addingBookmark {
				err := utils.AddBookmark(newBookmark, config.BookmarkStoreFile)
				if err != nil {
					fmt.Println("Error adding bookmark:", err)
				}
			}

			os.Exit(0)
		}
	}
}
