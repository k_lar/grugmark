package cli

import (
	"bufio"
	"encoding/json"
	"fmt"
	"grugmark/config"
	"grugmark/models"
	"grugmark/utils"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/pterm/pterm"
	"golang.design/x/clipboard"
)

func Help() {
	fmt.Println("Usage:")
	fmt.Println("  grugmark [options]")
	fmt.Println()
	fmt.Println("META OPTIONS:")
	fmt.Println("  -h, --help\t\t\tshow list of command-line options")
	fmt.Println("  -v, --version\t\t\tshow version of grugmark")
	fmt.Println()
	fmt.Println("BOOKMARK OPTIONS:")
	fmt.Println("  -a, --show-all\t\tshow all bookmarks stored")
	fmt.Println("  -s, --search\t\tinteractive fuzzy search")
	fmt.Println("  -n, --name\t\t\tdefine a bookmark name")
	fmt.Println("  -l, --link\t\t\tdefine the bookmark link")
	fmt.Println("  -d, --description\t\tdefine a bookmark's description")
	fmt.Println("  -c, --category\t\tdefine a bookmark's category")
	fmt.Println("  -t, --tags\t\t\tdefine a bookmark's tags (space seperated)")
}

func ShowVersion(version string) {
	fmt.Println("v" + version)
}

// AddName prompts the user to add a name to the bookmark
func AddName(b *models.Bookmark) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the bookmark name: ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text) // Remove any surrounding whitespace including the newline.
	b.Name = text

	if b.Name == "" {
		fmt.Println("A name is required to create a bookmark!")
		AddName(b)
	}
}

// AddDescription prompts the user to add a description to the bookmark
func AddDescription(b *models.Bookmark) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the bookmark description: ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text) // Remove any surrounding whitespace including the newline.
	b.Description = text
}

// AddTags prompts the user to add tags to the bookmark
func AddTags(b *models.Bookmark) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter tags (space-separated): ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text) // Remove any surrounding whitespace including the newline.
	b.Tags = strings.Fields(text)
}

// AddCategory prompts the user to add a category to the bookmark
func AddCategory(b *models.Bookmark) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the bookmark category: ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text) // Remove any surrounding whitespace including the newline.
	b.Category = text
}

func isLink(text string) bool {
	// Regular expression to match links starting with http:// or https://
	linkPattern := `^(http://|https://)`

	// Compile the regular expression
	re := regexp.MustCompile(linkPattern)

	// Check if the clipboard content matches the link pattern
	return re.MatchString(text)
}

func AddLink(b *models.Bookmark) {
	clipboard.Init()
	clipboardContent := clipboard.Read(clipboard.FmtText)

	if isLink(strings.TrimSpace(string(clipboardContent))) {
		if utils.YesOrNo("Take link from clipboard? : " + string(clipboardContent)) {
			b.Link = string(clipboardContent)
		}
	} else {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter the bookmark link: ")
		text, _ := reader.ReadString('\n')
		text = strings.TrimSpace(text) // Remove any surrounding whitespace including the newline.
		b.Link = text
		if b.Link == "" {
			fmt.Println("A link is required to create a bookmark!")
			AddLink(b)
		}
	}
}

// DisplayBookmarks reads and displays bookmarks from the bookmarks.json file
func DisplayBookmarks(filePath string) {
	err := utils.EnsureFilePathExists(filePath)
	if err != nil {
		fmt.Println("Error ensuring file path:", err)
		return
	}

	data, err := os.ReadFile(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Println("No bookmarks found.")
			return
		}
		fmt.Println("Error reading file:", err)
		return
	}

	var bookmarks models.Bookmarks
	err = json.Unmarshal(data, &bookmarks)
	if err != nil {
		fmt.Println("Error unmarshaling JSON:", err)
		return
	}

	if len(bookmarks) == 0 {
		fmt.Println("No bookmarks found.")
		return
	}

	for i, bookmark := range bookmarks {
		fmt.Printf("=== Bookmark %d: ===\n", i+1)
		fmt.Printf("Name: %s\n", bookmark.Name)
		fmt.Printf("Link: %s\n", bookmark.Link)
		if bookmark.Description != "" {
			fmt.Printf("Description: %s\n", bookmark.Description)
		}
		if len(bookmark.Tags) != 0 {
			fmt.Printf("Tags: %s\n", strings.Join(bookmark.Tags, ", "))
		}
		if bookmark.Category != "" {
			fmt.Printf("Category: %s\n", bookmark.Category)
		}
		fmt.Println()
	}
}

func AssistedMode() {
	config.InitConfig()
	var newBookmark models.Bookmark

	AddLink(&newBookmark)
	AddName(&newBookmark)
	AddDescription(&newBookmark)
	AddCategory(&newBookmark)
	AddTags(&newBookmark)

	err := utils.AddBookmark(newBookmark, config.BookmarkStoreFile)
	if err != nil {
		log.Fatal("Could not save bookmark: ", err)
	}

	fmt.Println()
	fmt.Println("Name:", newBookmark.Name)
	fmt.Println("Link:", newBookmark.Link)
	if newBookmark.Description != "" {
		fmt.Println("Description:", newBookmark.Description)
	}
	if len(newBookmark.Tags) != 0 {
		fmt.Println("Tags:", strings.Join(newBookmark.Tags, ", "))
	}
	if newBookmark.Category != "" {
		fmt.Println("Category:", newBookmark.Category)
	}
	fmt.Println()
}

func InteractiveSearchMode(filePath string) {
	err := utils.EnsureFilePathExists(filePath)
	if err != nil {
		fmt.Println("Error ensuring file path:", err)
		return
	}

	data, err := os.ReadFile(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Println("No bookmarks found.")
			return
		}
		fmt.Println("Error reading file:", err)
		return
	}

	var bookmarks models.Bookmarks
	err = json.Unmarshal(data, &bookmarks)
	if err != nil {
		fmt.Println("Error unmarshaling JSON:", err)
		return
	}

	if len(bookmarks) == 0 {
		fmt.Println("No bookmarks found.")
		return
	}

	var names []string
	for _, bookmark := range bookmarks {
		names = append(names, bookmark.Name)
	}

	selectedName, _ := pterm.DefaultInteractiveSelect.WithOptions(names).Show()
	fmt.Println()

	for _, bookmark := range bookmarks {
		if bookmark.Name == selectedName {
			fmt.Println("Name:", bookmark.Name)
			fmt.Println("Link:", bookmark.Link)
			if bookmark.Description != "" {
				fmt.Println("Description:", bookmark.Description)
			}
			if len(bookmark.Tags) != 0 {
				fmt.Println("Tags:", strings.Join(bookmark.Tags, ", "))
			}
			if bookmark.Category != "" {
				fmt.Println("Category:", bookmark.Category)
			}
			fmt.Println()
		}
	}
}
