package utils

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func GetEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}

func YesOrNo(s string) bool {
	r := bufio.NewReader(os.Stdin)

	fmt.Printf("%s [Y/n]: ", s)

	res, err := r.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}

	// Empty input (i.e., "\n") defaults to "yes"
	if len(res) < 2 {
		return true
	}

	input := strings.ToLower(strings.TrimSpace(res))

	if input[0] == 'y' {
		return true
	} else if input[0] == 'n' {
		return false
	}

	// Invalid input
	fmt.Println("Invalid input. Defaulting to 'yes'.")
	return true
}
