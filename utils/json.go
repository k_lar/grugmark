package utils

import (
	"encoding/json"
	"fmt"
	"grugmark/models"
	"os"
	"path/filepath"
)

// ConvertToJSON converts a Bookmarks slice to JSON
func ConvertToJSON(bookmarks models.Bookmarks) ([]byte, error) {
	return json.MarshalIndent(bookmarks, "", "  ")
}

// ConvertFromJSON converts JSON to a Bookmarks slice
func ConvertFromJSON(data []byte) (models.Bookmarks, error) {
	var bookmarks models.Bookmarks
	err := json.Unmarshal(data, &bookmarks)
	return bookmarks, err
}

func EnsureFilePathExists(filePath string) error {
	// Get the directory of the file path
	dir := filepath.Dir(filePath)

	// Check if the directory exists
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return fmt.Errorf("error creating directory: %v", err)
		}
	}

	// Check if the file exists
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		file, err := os.Create(filePath)
		if err != nil {
			return fmt.Errorf("error creating file: %v", err)
		}
		defer file.Close()
		fmt.Fprint(file, "[]")
	}

	// File path exists or has been created successfully
	return nil
}

func ValidateJSONFile(filePath string) error {
	// Read the file content
	fileContent, err := os.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("error reading file: %v", err)
	}

	// Attempt to unmarshal the JSON content into the expected type
	var bookmarks []models.Bookmark
	err = json.Unmarshal(fileContent, &bookmarks)
	if err != nil {
		return fmt.Errorf("error unmarshaling JSON: %v", err)
	}

	// JSON is correctly formatted
	return nil
}

func ValidateBookmark(b models.Bookmark) error {
	if b.Name == "" {
		return fmt.Errorf("Name is missing\n\tAdd with -n, --name \"Bookmark name\" ")
	}
	if b.Link == "" {
		return fmt.Errorf("Link is missing\n\tAdd with -l, --link \"https://example.com\" ")
	}
	// if b.Description == "" {
	// 	return fmt.Errorf("Description is missing\n\tAdd with -d, --description \"Bookmark description\" ")
	// }
	// if b.Category == "" {
	// 	return fmt.Errorf("Category is missing\n\tAdd with -c, --category \"Category\" ")
	// }
	// if len(b.Tags) == 0 {
	// 	return fmt.Errorf("Tags are missing\n\tAdd with -t, --tags \"tag1 tag2 tag4\" ")
	// }
	return nil
}

func AddBookmark(newBookmark models.Bookmark, bookmarkStore string) error {
	if err := ValidateBookmark(newBookmark); err != nil {
		return err
	}

	err := EnsureFilePathExists(bookmarkStore)
	if err != nil {
		return err
	}

	data, err := os.ReadFile(bookmarkStore)
	if err != nil {
		return err
	}

	var existingBookmarks models.Bookmarks
	err = json.Unmarshal(data, &existingBookmarks)
	if err != nil {
		return err
	}

	existingBookmarks = append(existingBookmarks, newBookmark)

	// Write the updated bookmarks to the file
	file, err := os.Create(bookmarkStore)
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	if err := encoder.Encode(existingBookmarks); err != nil {
		return err
	}

	return nil
}
