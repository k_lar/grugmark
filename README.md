# grugmark: a bookmark manager for smooth brained organisms

I didn't like how any other bookmark storage solution, so I made a simple one for myself.  
Mainly I just wanted it to work with my dotfiles, so I made it tailored just for that.

## Features

- Creating bookmarks (Wow!)
- Automatically grab links from clipboard (Y/n question)
- Get your bookmarks in a simple bookmarks.json file
    - Linux/BSD/Darwin: Located at `$HOME/.config/grugmark/bookmarks.json`
    - Windows: Located at the user's home folder in `grugmark/bookmarks.json`
    - Or set with an environment variable: `$GRUGMARK_PATH`

## Installation

Create binary in the local directory:  
```
make
```

To install it systemwide:
```
sudo make install
```  

## Usage

You can use grugmark with CLI arguments or in a simple "guided" mode.  

### TUI usage (guided mode)

This mode will guide you through creating a single bookmark.  
_Note: Only the link and name are required_  

```bash
grugmark
```

![guided mode example](https://gitlab.com/k_lar/grugmark/uploads/36f85de23cc4e77a66931ff67cdbfba8/tui.png)

### CLI usage

```bash
# Required arguments (link and name)
grugmark --link "https://example.com" --name "Example link"

# The same as above but shorter
grugmark -l "https://example.com" -n "Example link"

# You can add more fields to organize your bookmarks better
grugmark --link "https://example.com" --name "Example link" --description "That's a nice example right there" --category "Dev stuff" --tags "dev stupid examples"

# The same as above but shorter (separate tags with spaces!)
grugmark -l "https://example.com" -n "Example link" -d "That's a nice example right there" -c "Dev stuff" -t "dev stupid examples"

# To show bookmarks
grugmark -a

# Search bookmark names interactively
grugmark -s
```

![cli usage example](https://gitlab.com/k_lar/grugmark/uploads/5b3647fbf086d8e9d84f9a7accc031c8/cli.png)

## JSON bookmarks format:

Example file:

```json
[
  {
    "name": "tldraw",
    "link": "https://www.tldraw.com",
    "description": "A free and nice whiteboard",
    "tags": null,
    "category": "Cool stuff"
  },
  {
    "name": "My blog",
    "link": "https://k-lar.github.io/klarspace",
    "description": "",
    "tags": [
      "personal"
    ],
    "category": "My stuff"
  },
  {
    "name": "crem",
    "link": "https://gitlab.com/k_lar/crem",
    "description": "My worst creation",
    "tags": [
      "personal",
      "c"
    ],
    "category": "Dev"
  }
]
```
