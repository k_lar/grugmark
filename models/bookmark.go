package models

// Bookmark represents a bookmark entry
type Bookmark struct {
	Name        string   `json:"name"`
	Link        string   `json:"link"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
	Category    string   `json:"category"`
}

// Bookmarks represents a collection of bookmarks
type Bookmarks []Bookmark
