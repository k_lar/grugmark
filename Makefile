PREFIX ?= /usr
BINARY_NAME=grugmark

all: build

build:
	go build -o ${BINARY_NAME} main.go

run:
	go build -o ${BINARY_NAME} main.go
	./${BINARY_NAME}

install:
	@install -Dm755 grugmark $(DESTDIR)$(PREFIX)/bin/grugmark

uninstall:
	@rm -f $(DESTDIR)$(PREFIX)/bin/grugmark

clean:
	go clean
	rm ${BINARY_NAME}

